from lynx_label_maker.app import main

def init() -> None:
    if __name__ == "__main__":
        main()


init()
