#!/usr/bin/env python3


def main() -> None:
    print("Hello world!")


def init() -> None:
    if __name__ == "__main__":
        main()


init()
